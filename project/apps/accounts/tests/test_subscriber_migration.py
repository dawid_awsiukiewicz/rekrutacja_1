from unittest.mock import patch

from django.test import TestCase
from django.utils import timezone

from project.apps.accounts.models import Client, Subscriber, SubscriberSMS, User
from project.apps.accounts.subscriber_migration import (
    migrate_subscribers,
    migrate_subscribers_sms,
    set_gdpr_consent,
)


class MigrateSubscribersTestCase(TestCase):
    def setUp(self):
        for i in range(0, 3):
            Subscriber.objects.create(
                email=f"foo{i}@boo.se",
                gdpr_consent=True,
                create_date=timezone.now(),
            )

    def test_skip_create_when_user_exists(self):
        User.objects.create(email="foo1@boo.se", create_date=timezone.now())
        self.assertEqual(User.objects.count(), 1)
        migrate_subscribers()
        self.assertEqual(User.objects.count(), 3)

    def test_create_user_from_client(self):
        client_email = "foo1@boo.se"
        client = Client.objects.create(
            email=client_email, phone="0123456789", create_date=timezone.now()
        )
        migrate_subscribers()
        user = User.objects.get(email=client_email)
        subscriber = Subscriber.objects.get(email=client_email)
        self.assertEqual(user.email, client.email)
        self.assertEqual(user.phone, client.phone)
        self.assertEqual(user.gdpr_consent, subscriber.gdpr_consent)

    @patch("project.apps.accounts.subscriber_migration.generate_csv_file_from_json")
    def test_create_user_from_client_conflicts(self, mock_generate_csv):
        client = Client.objects.create(
            email="foo1@boo.se", phone="0123456789", create_date=timezone.now()
        )
        User.objects.create(
            email="foo2@boo.pl", phone=client.phone, create_date=timezone.now()
        )
        migrate_subscribers()
        mock_generate_csv.assert_called_once_with(
            [
                {"id": 2, "email": "foo1@boo.se"},
            ],
            "subscriber_conflicts.csv",
        )

    def test_client_does_not_exists(self):
        migrate_subscribers()
        self.assertEqual(Client.objects.count(), 0)
        self.assertEqual(Subscriber.objects.count(), 3)
        self.assertEqual(User.objects.count(), 3)
        for user in User.objects.all():
            subscriber = Subscriber.objects.get(email=user.email)
            self.assertEqual(user.email, subscriber.email)
            self.assertEqual(user.phone, "")
            self.assertEqual(user.gdpr_consent, subscriber.gdpr_consent)


class MigrateSubscribersSMSTestCase(TestCase):
    def setUp(self):
        for i in range(0, 3):
            SubscriberSMS.objects.create(
                phone=f"012345678{i}",
                gdpr_consent=True,
                create_date=timezone.now(),
            )

    def test_skip_create_when_user_exists(self):
        User.objects.create(phone="0123456781", create_date=timezone.now())
        self.assertEqual(User.objects.count(), 1)
        migrate_subscribers_sms()
        self.assertEqual(User.objects.count(), 3)

    def test_create_user_from_client(self):
        client_phone = "0123456781"
        client = Client.objects.create(
            email="foo@boo.pl", phone=client_phone, create_date=timezone.now()
        )
        migrate_subscribers_sms()
        user = User.objects.get(phone=client_phone)
        subscriber = SubscriberSMS.objects.get(phone=client_phone)
        self.assertEqual(user.email, client.email)
        self.assertEqual(user.phone, client.phone)
        self.assertEqual(user.gdpr_consent, subscriber.gdpr_consent)

    @patch("project.apps.accounts.subscriber_migration.generate_csv_file_from_json")
    def test_create_user_from_client_conflicts(self, mock_generate_csv):
        client = Client.objects.create(
            email="foo1@boo.se", phone="0123456781", create_date=timezone.now()
        )
        User.objects.create(
            email=client.email, phone="0123456782", create_date=timezone.now()
        )
        migrate_subscribers_sms()
        mock_generate_csv.assert_called_once_with(
            [
                {"id": 2, "phone": "0123456781"},
            ],
            "subscriber_sms_conflicts.csv",
        )

    @patch("project.apps.accounts.subscriber_migration.generate_csv_file_from_json")
    def test_create_user_from_client_duplicated_phone_number(self, mock_generate_csv):
        Client.objects.create(
            email="foo1@boo.se", phone="0123456781", create_date=timezone.now()
        )
        Client.objects.create(
            email="foo2@boo.se", phone="0123456781", create_date=timezone.now()
        )
        migrate_subscribers_sms()
        mock_generate_csv.assert_called_once_with(
            [
                {"id": 2, "phone": "0123456781"},
            ],
            "subscriber_sms_conflicts.csv",
        )

    def test_client_does_not_exists(self):
        migrate_subscribers_sms()
        self.assertEqual(Client.objects.count(), 0)
        self.assertEqual(SubscriberSMS.objects.count(), 3)
        self.assertEqual(User.objects.count(), 3)
        for user in User.objects.all():
            subscriber = SubscriberSMS.objects.get(phone=user.phone)
            self.assertEqual(user.email, "")
            self.assertEqual(user.phone, subscriber.phone)
            self.assertEqual(user.gdpr_consent, subscriber.gdpr_consent)


class SetGDPRConsentTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            email="foo@boo.pl",
            phone="0123456789",
            create_date="2021-2-1",
            gdpr_consent=False,
        )

    def test_newer_subscriber_does_not_exists(self):
        SubscriberSMS.objects.create(
            phone="0123456789",
            gdpr_consent=True,
            create_date="2020-2-1",
        )
        Subscriber.objects.create(
            email="foo@boo.pl",
            gdpr_consent=True,
            create_date="2020-2-1",
        )
        set_gdpr_consent()
        user = User.objects.get(email=self.user.email)
        self.assertEqual(user.gdpr_consent, False)

    def test_newer_subscriber_exists(self):
        Subscriber.objects.create(
            email="foo@boo.pl",
            gdpr_consent=True,
            create_date="2021-3-1",
        )
        set_gdpr_consent()
        user = User.objects.get(email=self.user.email)
        self.assertEqual(user.gdpr_consent, True)

    def test_newer_subscriber_sms_exists(self):
        SubscriberSMS.objects.create(
            phone="0123456789",
            gdpr_consent=True,
            create_date="2021-3-1",
        )
        set_gdpr_consent()
        user = User.objects.get(email=self.user.email)
        self.assertEqual(user.gdpr_consent, True)

    def test_newer_subscriber_than_subscriber_sms_exists(self):
        SubscriberSMS.objects.create(
            phone="0123456789",
            gdpr_consent=False,
            create_date="2021-3-1",
        )
        Subscriber.objects.create(
            email="foo@boo.pl",
            gdpr_consent=True,
            create_date="2021-3-2",
        )
        set_gdpr_consent()
        user = User.objects.get(email=self.user.email)
        self.assertEqual(user.gdpr_consent, True)

    def test_newer_subscriber_sms_than_subscriber_exists(self):
        SubscriberSMS.objects.create(
            phone="0123456789",
            gdpr_consent=True,
            create_date="2021-3-4",
        )
        Subscriber.objects.create(
            email="foo@boo.pl",
            gdpr_consent=False,
            create_date="2021-3-2",
        )
        set_gdpr_consent()
        user = User.objects.get(email=self.user.email)
        self.assertEqual(user.gdpr_consent, True)
