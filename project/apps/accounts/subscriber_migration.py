from typing import List

from project.apps.accounts.exceptions import (
    ClientPhoneNumberNotUniqueException,
    SubscriberConflictsException,
    SubscriberDoesNotExist,
    UserAlreadyExists,
)
from project.apps.accounts.models import Client, Subscriber, SubscriberSMS, User
from project.utils.files import generate_csv_file_from_json


def migrate_subscribers():
    conflicts = []
    users_to_create = []
    for subscriber in Subscriber.objects.all():
        try:
            user = _migrate_subscriber_to_user(subscriber)
        except SubscriberConflictsException as err:
            conflicts.append(err.data)
        except UserAlreadyExists:
            print(f"User {subscriber.email} already exists")
        else:
            users_to_create.append(user)

    _create_users(users_to_create)

    if conflicts:
        generate_csv_file_from_json(conflicts, "subscriber_conflicts.csv")


def migrate_subscribers_sms():
    conflicts = []
    user_to_create = []
    for subscriber in SubscriberSMS.objects.all():
        try:
            user = _migrate_subscriber_sms_to_user(subscriber)
        except SubscriberConflictsException as err:
            conflicts.append(err.data)
        except ClientPhoneNumberNotUniqueException as err:
            conflicts.append(err.data)
        except UserAlreadyExists:
            print(f"User {subscriber.phone} already exists")
        else:
            user_to_create.append(user)

    _create_users(user_to_create)
    if conflicts:
        generate_csv_file_from_json(conflicts, "subscriber_sms_conflicts.csv")


def set_gdpr_consent():
    users_to_update = []
    for user in User.objects.all():
        try:
            updated_user = _set_gdpr_consent_for_user(user)
            users_to_update.append(updated_user)
        except SubscriberDoesNotExist:
            print(f"Newer subscriber related with user {user.email} doesn't exists")

    _update_users(users_to_update)


def _set_gdpr_consent_for_user(user: User) -> User:
    subscriber_sms = SubscriberSMS.objects.filter(
        phone=user.phone, create_date__gt=user.create_date
    ).first()
    subscriber = Subscriber.objects.filter(
        email=user.email, create_date__gt=user.create_date
    ).first()

    if not any([subscriber, subscriber_sms]):
        raise SubscriberDoesNotExist

    if all([subscriber, subscriber_sms]):
        gdpr_consent = (
            subscriber.gdpr_consent
            if subscriber.create_date > subscriber_sms.create_date
            else subscriber_sms.gdpr_consent
        )
    elif subscriber_sms:
        gdpr_consent = subscriber_sms.gdpr_consent
    else:
        gdpr_consent = subscriber.gdpr_consent

    user.gdpr_consent = gdpr_consent
    return user


def _migrate_subscriber_to_user(subscriber: Subscriber) -> User:
    if User.objects.filter(email=subscriber.email).exists():
        raise UserAlreadyExists()
    try:
        client = Client.objects.get(email=subscriber.email)
    except Client.DoesNotExist:
        user = _user_from_subscriber(subscriber)
    else:
        user_exists = (
            User.objects.filter(phone=client.phone).exclude(email=client.email).exists()
        )
        if user_exists:
            raise SubscriberConflictsException(
                {"id": subscriber.id, "email": subscriber.email}
            )
        user = _user_from_client(client, subscriber.gdpr_consent)
    return user


def _migrate_subscriber_sms_to_user(subscriber: SubscriberSMS) -> User:
    if User.objects.filter(phone=subscriber.phone).exists():
        raise UserAlreadyExists()

    try:
        client = Client.objects.get(phone=subscriber.phone)
    except Client.DoesNotExist:
        user = _user_from_subscriber_sms(subscriber)
    except Client.MultipleObjectsReturned:
        raise ClientPhoneNumberNotUniqueException(
            {"id": subscriber.id, "phone": subscriber.phone}
        )
    else:
        user_exists = (
            User.objects.filter(email=client.email).exclude(phone=client.phone).exists()
        )
        if user_exists:
            raise SubscriberConflictsException(
                {"id": subscriber.id, "phone": subscriber.phone}
            )
        user = _user_from_client(client, subscriber.gdpr_consent)
    return user


def _create_users(users_to_create: List[User]) -> None:
    User.objects.bulk_create(users_to_create, batch_size=200)


def _update_users(users_to_update: List[User]) -> None:
    User.objects.bulk_update(users_to_update, ["gdpr_consent"], batch_size=200)


def _user_from_client(client: Client, gdpr_consent: bool) -> User:
    user = User(
        create_date=client.create_date,
        email=client.email,
        phone=client.phone,
        gdpr_consent=gdpr_consent,
    )
    return user


def _user_from_subscriber(subscriber: Subscriber) -> User:
    user = User(
        create_date=subscriber.create_date,
        email=subscriber.email,
        gdpr_consent=subscriber.gdpr_consent,
    )
    return user


def _user_from_subscriber_sms(subscriber: SubscriberSMS) -> User:
    user = User(
        create_date=subscriber.create_date,
        phone=subscriber.phone,
        gdpr_consent=subscriber.gdpr_consent,
    )
    return user
