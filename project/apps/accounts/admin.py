from django.contrib import admin

from project.apps.accounts.models import Client, Subscriber, SubscriberSMS, User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    pass


@admin.register(SubscriberSMS)
class SubscriberSMSAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass
