from django.db import models


class Subscriber(models.Model):
    create_date = models.DateTimeField("Create date")
    email = models.EmailField("Email", unique=True)
    gdpr_consent = models.BooleanField("GDPR")


class SubscriberSMS(models.Model):
    create_date = models.DateTimeField("Create date")
    phone = models.CharField("Phone", max_length=30, unique=True)
    gdpr_consent = models.BooleanField("GDPR")


class Client(models.Model):
    create_date = models.DateTimeField("Create date")
    email = models.EmailField("Email", unique=True, blank=True)
    phone = models.CharField("Phone", max_length=30, blank=True)


class User(models.Model):
    create_date = models.DateTimeField("Create date")
    email = models.EmailField("Email", blank=True)
    phone = models.CharField("Phone", max_length=30, blank=True)
    gdpr_consent = models.BooleanField("GDPR", null=True)
