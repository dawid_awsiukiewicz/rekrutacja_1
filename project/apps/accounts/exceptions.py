class SubscriberConflictsException(Exception):
    def __init__(self, data: dict):
        super().__init__()
        self.data = data


class UserAlreadyExists(Exception):
    pass


class SubscriberDoesNotExist(Exception):
    pass


class ClientPhoneNumberNotUniqueException(Exception):
    def __init__(self, data: dict):
        super().__init__()
        self.data = data
