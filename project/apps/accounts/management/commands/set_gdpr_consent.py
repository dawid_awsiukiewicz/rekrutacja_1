from django.core.management import BaseCommand

from project.apps.accounts.subscriber_migration import set_gdpr_consent


class Command(BaseCommand):
    def handle(self, *args, **options):
        set_gdpr_consent()
