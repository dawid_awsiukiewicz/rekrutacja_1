from django.core.management import BaseCommand

from project.apps.accounts.subscriber_migration import migrate_subscribers


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Converting subscribers sms to users...")
        migrate_subscribers()
