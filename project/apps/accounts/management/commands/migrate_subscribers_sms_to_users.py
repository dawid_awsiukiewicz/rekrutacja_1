from django.core.management import BaseCommand

from project.apps.accounts.subscriber_migration import migrate_subscribers_sms


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Converting subscribers to users...")
        migrate_subscribers_sms()
