import csv
from typing import List


def generate_csv_file_from_json(items: List[dict], file_name: str) -> None:
    data_file = open(file_name, "w")
    csv_writer = csv.writer(data_file)
    for count, item in enumerate(items):
        if count == 0:
            header = item.keys()
            csv_writer.writerow(header)
        csv_writer.writerow(item.values())
    data_file.close()
