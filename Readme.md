1. Configure dev environment

    ```
    $ pyenv virtualenv 3.9.0 env --copies
    $ pyenv activate env
    $ pip install -r requirements.txt
    ```

2. Available manage commands
    ```
    (env)$ ./manage.py migrate_subscribers_sms_to_users
    (env)$ ./manage.py migrate_subscribers_to_users
    (env)$ ./manage.py set_gdpr_consent
    ```
